# novnc and tightvncserver

Reference:
https://docs.dataplicity.com/docs/make-a-remote-graphical-interface-for-your-pi



```
sudo apt-get install tightvncserver
tightvncserver
```

The first time you run TightVNC server, you'll be asked to choose a password. Write it down when you enter it. Entering a view-only password is optional, so you can either input one or just type 'N' (no) to omit this.


# Create a script to start VNC
The command to run VNC manually is quite long and tedious, so we can create a script to save typing. At the Pi terminal, use a text editor to create a script called 'vnc.sh':

vnc.sh:

```

#!/bin/sh
vncserver :1 -geometry 1920x1080 -depth 24 -dpi 96

```

Make the script executable and run it:

```
sudo chmod +x vnc.sh
./vnc.sh
```

# Configure TightVNC server to start at boot

VNC is most convenient when it starts automatically at boot, so we'll start by making an "init-script" in a text editor.

We need superuser permission to add a system start script, so switch to superuser:

```
sudo su
```

Create a file called 'vncboot' in the system init directory:

/etc/init.d/vncboot:

```
#! /bin/sh
# /etc/init.d/vncboot

### BEGIN INIT INFO
# Provides: vncboot
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start VNC Server at boot time
# Description: Start VNC Server at boot time.
### END INIT INFO

USER=pi
HOME=/home/pi

export USER HOME

case "$1" in
 start)
  echo "Starting VNC Server"
  #Insert your favoured settings for a VNC session
  su - $USER -c "/usr/bin/vncserver :1 -geometry 1280x800 -depth 16 -pixelformat rgb565"
  ;;

 stop)
  echo "Stopping VNC Server"
  /usr/bin/vncserver -kill :1
  ;;

 *)
  echo "Usage: /etc/init.d/vncboot {start|stop}"
  exit 1
  ;;
esac

exit 0
```

Make the new init script executable and add it to the system boot list:

```
sudo chmod 755 vncboot
sudo update-rc.d vncboot defaults
```

# Install the noVNC client on your Pi

NoVNC is available from GitHub, and to install it you'll need 'git'. At the root prompt (or via sudo) type:

```
sudo apt-get install git websockify
```

Download noVNC and enter the directory:

```
git clone git://github.com/kanaka/noVNC
cd noVNC
```


Copy vnc.html to index.html:

```
cp vnc.html index.html
```

	
Launch websockify:


```
cd noVNC
./utils/launch.sh --vnc 127.0.0.1:5901 --listen 80
```

You can now navigate to e.g. raspberrypi.local (or IP address of your computer)

# Run websockify automatically at boot

Add a command to the end of the rc.local file:


``` 
sudo vi /etc/rc.local

# start novnc server
sudo /home/pi/noVNC/utils/launch.sh --vnc 127.0.0.1:5901 --listen 80 &

```







